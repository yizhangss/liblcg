/******************************************************
 * C++ Library of the Linear Conjugate Gradient Methods (LibLCG)
 * 
 * Copyright (C) 2022  Yi Zhang (yizhang-geo@zju.edu.cn)
 * 
 * LibLCG is distributed under a dual licensing scheme. You can
 * redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License (LGPL) as published by the Free Software Foundation,
 * either version 2 of the License, or (at your option) any later version. 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * If the terms and conditions of the LGPL v.2. would prevent you from
 * using the LibLCG, please consider the option to obtain a commercial
 * license for a fee. These licenses are offered by the LibLCG developing 
 * team. As a rule, licenses are provided "as-is", unlimited in time for 
 * a one time fee. Please send corresponding requests to: yizhang-geo@zju.edu.cn. 
 * Please do not forget to include some description of your company and the 
 * realm of its activities. Also add information on how to contact you by 
 * electronic and paper mail.
 ******************************************************/

#include "lcg_array.h"

lcg_array::lcg_array()
{
    len = 0;
    val = nullptr;
}

lcg_array::lcg_array(size_t l)
{
    len = 0;
    val = nullptr;
    resize(l);
}

lcg_array::~lcg_array()
{
    clear();
}

lcg_float &lcg_array::operator[](size_t index)
{
    return val[index];
}

lcg_float &lcg_array::operator[](size_t index) const
{
    return val[index];
}

void lcg_array::resize(size_t l)
{
    if (l == 0)
    {
        throw std::invalid_argument("Invalid array size. lcg_array::resize(...)");
    }

    if (len != l)
    {
        if (len != 0) clear();
        len = l;
        val = new lcg_float [len];
    }
    return;
}

void lcg_array::clear()
{
    len = 0;
    if (val != nullptr)
    {
        delete[] val;
        val = nullptr;
    }
    return;
}

size_t lcg_array::size() const
{
    return len;
}

void lcg_dot(lcg_float &dot, const lcg_array &a, const lcg_array &b)
{
    dot = 0;
    for (size_t i = 0; i < a.size(); i++)
    {
        dot += a[i]*b[i];
    }
    return;
}