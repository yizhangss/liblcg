/******************************************************
 * C++ Library of the Linear Conjugate Gradient Methods (LibLCG)
 * 
 * Copyright (C) 2022  Yi Zhang (yizhang-geo@zju.edu.cn)
 * 
 * LibLCG is distributed under a dual licensing scheme. You can
 * redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License (LGPL) as published by the Free Software Foundation,
 * either version 2 of the License, or (at your option) any later version. 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * If the terms and conditions of the LGPL v.2. would prevent you from
 * using the LibLCG, please consider the option to obtain a commercial
 * license for a fee. These licenses are offered by the LibLCG developing 
 * team. As a rule, licenses are provided "as-is", unlimited in time for 
 * a one time fee. Please send corresponding requests to: yizhang-geo@zju.edu.cn. 
 * Please do not forget to include some description of your company and the 
 * realm of its activities. Also add information on how to contact you by 
 * electronic and paper mail.
 ******************************************************/

#ifndef _LCG_ARRAY_H
#define _LCG_ARRAY_H

#ifndef LibLCG_ARRAY
#define LibLCG_ARRAY

#include "algebra.h"
#include "stddef.h"
#include "exception"
#include "stdexcept"

class lcg_array
{
protected:
    size_t len;
    lcg_float *val;

public:
    lcg_array();
    lcg_array(size_t l);
    virtual ~lcg_array();

    lcg_float &operator[](size_t id);
    lcg_float &operator[](size_t id) const;

    void resize(size_t l);
    void clear();
    size_t size() const;
};

#endif // LibLCG_ARRAY

void lcg_dot(lcg_float &dot, const lcg_array &a, const lcg_array &b);

#endif // _LCG_ARRAY_H