/******************************************************
 * C++ Library of the Linear Conjugate Gradient Methods (LibLCG)
 * 
 * Copyright (C) 2022  Yi Zhang (yizhang-geo@zju.edu.cn)
 * 
 * LibLCG is distributed under a dual licensing scheme. You can
 * redistribute it and/or modify it under the terms of the GNU Lesser
 * General Public License (LGPL) as published by the Free Software Foundation,
 * either version 2 of the License, or (at your option) any later version. 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * If the terms and conditions of the LGPL v.2. would prevent you from
 * using the LibLCG, please consider the option to obtain a commercial
 * license for a fee. These licenses are offered by the LibLCG developing 
 * team. As a rule, licenses are provided "as-is", unlimited in time for 
 * a one time fee. Please send corresponding requests to: yizhang-geo@zju.edu.cn. 
 * Please do not forget to include some description of your company and the 
 * realm of its activities. Also add information on how to contact you by 
 * electronic and paper mail.
 ******************************************************/

#include "lcg.h"

#include "cmath"

#include "config.h"
#ifdef LibLCG_OPENMP
#include "omp.h"
#endif

/**
 * @brief      Callback interface of the conjugate gradient solver
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 *
 * @return     Status of the function.
 */
typedef int (*lcg_solver_ptr)(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance);

//int lcg(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
//	const lcg_para* param, void* instance, lcg_float* Gk, lcg_float* Dk, lcg_float* ADk);
//int lcgs(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
//	const lcg_para* param, void* instance, lcg_float* RK, lcg_float* R0T, lcg_float* PK, lcg_float* AX, 
//	lcg_float* UK, lcg_float* QK, lcg_float* WK);
int lbicgstab(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance);
int lbicgstab2(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance);

int lcg_solver(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance, lcg_solver_enum solver_id)
{
	lcg_solver_ptr cg_solver;
	switch (solver_id)
	{
		case LCG_CG:
			return lcg(Afp, Pfp, m, B, param, instance, nullptr, nullptr, nullptr);
		case LCG_CGS:
			return lcgs(Afp, Pfp, m, B, param, instance, nullptr, nullptr, nullptr, 
				nullptr, nullptr, nullptr, nullptr);
		case LCG_BICGSTAB:
			cg_solver = lbicgstab;
			break;
		case LCG_BICGSTAB2:
			cg_solver = lbicgstab2;
			break;
		default:
			return lcgs(Afp, Pfp, m, B, param, instance, nullptr, nullptr, nullptr, 
				nullptr, nullptr, nullptr, nullptr);
	}

	return cg_solver(Afp, Pfp, m, B, param, instance);
}

int lpcg(lcg_axfunc_ptr Afp, lcg_axfunc_ptr Mfp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance);

int lcg_solver_preconditioned(lcg_axfunc_ptr Afp, lcg_axfunc_ptr Mfp, lcg_progress_ptr Pfp, lcg_array &m, 
	const lcg_array &B, const lcg_para* param, void* instance, lcg_solver_enum solver_id)
{
	return lpcg(Afp, Mfp, Pfp, m, B, param, instance);
}

/**
 * @brief      A combined conjugate gradient solver function.
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param[in]  low         The lower boundary of the acceptable solution.
 * @param[in]  hig         The higher boundary of the acceptable solution.
 * @param[in]  n_size      Size of the solution vector and objective vector.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 * @param      solver_id   Solver type used to solve the linear system. The default value is LCG_CGS.
 * @param      P           Precondition vector (optional expect for the LCG_PCG method). The default value is nullptr.
 *
 * @return     Status of the function.
 */
typedef int (*lcg_solver_ptr2)(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_array &low, const lcg_array &hig, const lcg_para* param, void* instance);

int lpg(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_array &low, const lcg_array &hig, const lcg_para* param, void* instance);
int lspg(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_array &low, const lcg_array &hig, const lcg_para* param, void* instance);

int lcg_solver_constrained(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_array &low, const lcg_array &hig, const lcg_para* param, void* instance, lcg_solver_enum solver_id)
{
	lcg_solver_ptr2 cg_solver;
	switch (solver_id)
	{
		case LCG_PG:
			cg_solver = lpg;
			break;
		case LCG_SPG:
			cg_solver = lspg;
			break;
		default:
			cg_solver = lpg;
			break;
	}

	return cg_solver(Afp, Pfp, m, B, low, hig, param, instance);
}


int lcg(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance, lcg_array *Gk, lcg_array *Dk, lcg_array *ADk)
{
	// set CG parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	//check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0 || para.epsilon >= 1.0) return LCG_INVILAD_EPSILON;

	// locate memory
	lcg_array gk, dk, Adk;
	if (Gk == nullptr) gk.resize(n_size); // this may not work
	else gk = *Gk;

	if (Dk == nullptr) dk.resize(n_size);
	else dk = *Dk;

	if (ADk == nullptr) Adk.resize(n_size);
	else Adk = *ADk;

	Afp(instance, m, Adk);

	size_t i;
#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		gk[i] = Adk[i] - B[i];
		dk[i] = -1.0*gk[i];
	}

	lcg_float gk_mod;
	lcg_dot(gk_mod, gk, gk);

	lcg_float g0_mod = gk_mod;
	if (g0_mod < 1.0) g0_mod = 1.0;

	int ret, t = 0;
	if (para.abs_diff && sqrt(gk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(gk_mod)/n_size, &para, 0);
		}
		return ret;
	}
	else if (gk_mod/g0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, gk_mod/g0_mod, &para, 0);
		}
		return ret;
	}

	lcg_float dTAd, ak, betak, gk1_mod, residual;
	while (1)
	{
		if (para.abs_diff) residual = sqrt(gk_mod)/n_size;
		else residual = gk_mod/g0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}
		
		t++;

		Afp(instance , dk, Adk);

		lcg_dot(dTAd, dk, Adk);
		ak = gk_mod/dTAd;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] += ak*dk[i];
			gk[i] += ak*Adk[i];
		}

		for (i = 0; i < n_size; i++)
		{
			if (m[i] != m[i])
			{
				ret = LCG_NAN_VALUE; return ret;
			}
		}

		lcg_dot(gk1_mod, gk, gk);
		betak = gk1_mod/gk_mod;
		gk_mod = gk1_mod;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			dk[i] = betak*dk[i] - gk[i];
		}
	}

	return ret;
}

/**
 * @brief      Preconditioned conjugate gradient method
 * 
 * @note       Algorithm 1 in "Preconditioned conjugate gradients for singular systems" by Kaasschieter (1988).
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Mfp         Callback function for calculating the product of 'M^{-1}x', in which M is the preconditioning matrix.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param[in]  n_size      Size of the solution vector and objective vector.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 *
 * @return     Status of the function.
 */
int lpcg(lcg_axfunc_ptr Afp, lcg_axfunc_ptr Mfp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, const lcg_para* param, void* instance)
{
	// set CG parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	//check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0 || para.epsilon >= 1.0) return LCG_INVILAD_EPSILON;

	// locate memory
	lcg_array rk, zk, dk, Adk;
	rk.resize(n_size); zk.resize(n_size);
	dk.resize(n_size); Adk.resize(n_size);

	Afp(instance, m, Adk);

	size_t i;
#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		rk[i] = B[i] - Adk[i];
	}

	Mfp(instance, rk, zk);

#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		dk[i] = zk[i];
	}

	lcg_float rk_mod;
	lcg_dot(rk_mod, rk, rk);

	lcg_float r0_mod = rk_mod;
	if (r0_mod < 1.0) r0_mod = 1.0;

	lcg_float zTr;
	lcg_dot(zTr, zk, rk);

	int ret, t = 0;
	if (para.abs_diff && sqrt(rk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(rk_mod)/n_size, &para, 0);
		}
		return ret;
	}
	else if (rk_mod/r0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, rk_mod/r0_mod, &para, 0);
		}
		return ret;
	}

	lcg_float dTAd, ak, betak, zTr1, residual;
	while (1)
	{
		if (para.abs_diff) residual = sqrt(rk_mod)/n_size;
		else residual = rk_mod/r0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}
		
		t++;

		Afp(instance , dk, Adk);

		lcg_dot(dTAd, dk, Adk);
		ak = zTr/dTAd;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] += ak*dk[i];
			rk[i] -= ak*Adk[i];
		}

		Mfp(instance, rk, zk);

		lcg_dot(rk_mod, rk, rk);

		for (i = 0; i < n_size; i++)
		{
			if (m[i] != m[i])
			{
				ret = LCG_NAN_VALUE; return ret;
			}
		}

		lcg_dot(zTr1, zk, rk);
		betak = zTr1/zTr;
		zTr = zTr1;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			dk[i] = zk[i] + betak*dk[i];
		}
	}

	return ret;
}


int lcgs(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, const lcg_para* param, 
	void* instance, lcg_array *RK, lcg_array *R0T, lcg_array *PK, lcg_array *AX, 
	lcg_array *UK, lcg_array *QK, lcg_array *WK)
{
	// set CGS parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	//check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0 || para.epsilon >= 1.0) return LCG_INVILAD_EPSILON;

	size_t i;
	lcg_array rk, r0_T, pk;
	lcg_array Ax, uk, qk, wk;
	if ( RK == nullptr) rk.resize(n_size);
	else rk = *RK;

	if ( R0T == nullptr) r0_T.resize(n_size);
	else r0_T = *R0T;

	if ( PK == nullptr) pk.resize(n_size);
	else pk = *PK;

	if ( AX == nullptr) Ax.resize(n_size);
	else Ax = *AX;

	if ( UK == nullptr) uk.resize(n_size);
	else uk = *UK;

	if ( QK == nullptr) qk.resize(n_size);
	else qk = *QK;

	if ( WK == nullptr) wk.resize(n_size);
	else wk = *WK;

	Afp(instance, m, Ax);

	// 假设p0和q0为零向量 则在第一次迭代是pk和uk都等于rk
	// 所以我们能直接从计算Apk开始迭代
#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		pk[i] = uk[i] = r0_T[i] = rk[i] = B[i] - Ax[i];
	}

	lcg_float rkr0_T = 0.0;
	for (i = 0; i < n_size; i++)
	{
		rkr0_T += rk[i]*r0_T[i];
	}

	lcg_float rk_mod;
	lcg_dot(rk_mod, rk, rk);

	lcg_float r0_mod = rk_mod;
	if (r0_mod < 1.0) r0_mod = 1.0;

	int ret, t = 0;
	if (para.abs_diff && sqrt(rk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(rk_mod)/n_size, &para, 0);
		}
		return ret;
	}	
	else if (rk_mod/r0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, rk_mod/r0_mod, &para, 0);
		}
		return ret;
	}

	lcg_float ak, rk_abs, rkr0_T1, Apr_T, betak, residual;
	while (1)
	{
		if (para.abs_diff) residual = sqrt(rk_mod)/n_size;
		else residual = rk_mod/r0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}
		
		t++;

		Afp(instance, pk, Ax);

		Apr_T = 0.0;
		for (i = 0; i < n_size; i++)
		{
			Apr_T  += Ax[i]*r0_T[i];
		}
		ak = rkr0_T/Apr_T;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			qk[i] = uk[i] - ak*Ax[i];
			wk[i] = uk[i] + qk[i];
		}

		Afp(instance, wk, Ax);

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] += ak*wk[i];
			rk[i] -= ak*Ax[i];
		}

		lcg_dot(rk_mod, rk, rk);

		for (i = 0; i < n_size; i++)
		{
			if (m[i] != m[i])
			{
				ret = LCG_NAN_VALUE; return ret;
			}
		}

		rkr0_T1 = 0.0;
		for (i = 0; i < n_size; i++)
		{
			rkr0_T1 += rk[i]*r0_T[i];
		}
		betak = rkr0_T1/rkr0_T;
		rkr0_T = rkr0_T1;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			uk[i] = rk[i] + betak*qk[i];
			pk[i] = uk[i] + betak*(qk[i] + betak*pk[i]);
		}
	}

	return ret;
}

/**
 * @brief      Biconjugate gradient method.
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param[in]  n_size      Size of the solution vector and objective vector.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 * @param      P           Precondition vector (optional expect for the LCG_PCG method). The default value is nullptr.
 *
 * @return     Status of the function.
 */
int lbicgstab(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance)
{
	// set CGS parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	//check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0 || para.epsilon >= 1.0) return LCG_INVILAD_EPSILON;

	size_t i;
	lcg_array rk, r0_T, pk;
	lcg_array Ax, sk, Apk;
	rk.resize(n_size); r0_T.resize(n_size);
	pk.resize(n_size); Ax.resize(n_size);
	sk.resize(n_size); Apk.resize(n_size);

	Afp(instance, m, Ax);

#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		pk[i] = r0_T[i] = rk[i] = B[i] - Ax[i];
	}

	lcg_float rkr0_T = 0.0;
	for (i = 0; i < n_size; i++)
	{
		rkr0_T += rk[i]*r0_T[i];
	}

	lcg_float rk_mod;
	lcg_dot(rk_mod, rk, rk);

	lcg_float r0_mod = rk_mod;
	if (r0_mod < 1.0) r0_mod = 1.0;

	int ret, t = 0;
	if (para.abs_diff && sqrt(rk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(rk_mod)/n_size, &para, 0);
		}
		return ret;
	}	
	else if (rk_mod/r0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, rk_mod/r0_mod, &para, 0);
		}
		return ret;
	}

	lcg_float ak, wk, rkr0_T1, Apr_T, betak, Ass, AsAs, residual;
	while(1)
	{
		if (para.abs_diff) residual = sqrt(rk_mod)/n_size;
		else residual = rk_mod/r0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}
		
		t++;

		Afp(instance, pk, Apk);

		Apr_T = 0.0;
		for (i = 0; i < n_size; i++)
		{
			Apr_T  += Apk[i]*r0_T[i];
		}
		ak = rkr0_T/Apr_T;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			sk[i] = rk[i] - ak*Apk[i];
		}

		Afp(instance, sk, Ax);

		Ass = AsAs = 0.0;
		for (i = 0; i < n_size; i++)
		{
			Ass  += Ax[i]*sk[i];
			AsAs += Ax[i]*Ax[i];
		}
		wk = Ass/AsAs;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] += (ak*pk[i] + wk*sk[i]);
		}

		for (i = 0; i < n_size; i++)
		{
			if (m[i] != m[i])
			{
				ret = LCG_NAN_VALUE; return ret;
			}
		}

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			rk[i] = sk[i] - wk*Ax[i];
		}

		lcg_dot(rk_mod, rk, rk);

		rkr0_T1 = 0.0;
		for (i = 0; i < n_size; i++)
		{
			rkr0_T1 += rk[i]*r0_T[i];
		}
		betak = (ak/wk)*rkr0_T1/rkr0_T;
		rkr0_T = rkr0_T1;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			pk[i] = rk[i] + betak*(pk[i] - wk*Apk[i]);
		}
	}

	return ret;
}


/**
 * @brief      Biconjugate gradient method 2.
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param[in]  n_size      Size of the solution vector and objective vector.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 * @param      P           Precondition vector (optional expect for the LCG_PCG method). The default value is nullptr.
 *
 * @return     Status of the function.
 */
int lbicgstab2(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_para* param, void* instance)
{
	// set CGS parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	//check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0) return LCG_INVILAD_EPSILON;
	if (para.restart_epsilon <= 0.0 || para.epsilon >= 1.0) return LCG_INVILAD_RESTART_EPSILON;

	size_t i;
	lcg_array rk, r0_T, pk;
	lcg_array Ax, sk, Apk;
	rk.resize(n_size); r0_T.resize(n_size);
	pk.resize(n_size); Ax.resize(n_size);
	sk.resize(n_size); Apk.resize(n_size);

	Afp(instance, m, Ax);

#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		pk[i] = r0_T[i] = rk[i] = B[i] - Ax[i];
	}

	lcg_float rkr0_T = 0.0;
	for (i = 0; i < n_size; i++)
	{
		rkr0_T += rk[i]*r0_T[i];
	}

	lcg_float rk_mod;
	lcg_dot(rk_mod, rk, rk);

	lcg_float r0_mod = rk_mod;
	if (r0_mod < 1.0) r0_mod = 1.0;

	int ret, t = 0;
	if (para.abs_diff && sqrt(rk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(rk_mod)/n_size, &para, 0);
		}
		return ret;
	}	
	else if (rk_mod/r0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, rk_mod/r0_mod, &para, 0);
		}
		return ret;
	}

	lcg_float ak, wk, rk_abs, rkr0_T1, Apr_T, betak;
	lcg_float Ass, AsAs, rr1_abs, residual;
	while(1)
	{
		if (para.abs_diff) residual = sqrt(rk_mod)/n_size;
		else residual = rk_mod/r0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}
		
		t++;

		Afp(instance, pk, Apk);

		Apr_T = 0.0;
		for (i = 0; i < n_size; i++)
		{
			Apr_T  += Apk[i]*r0_T[i];
		}
		ak = rkr0_T/Apr_T;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			sk[i] = rk[i] - ak*Apk[i];
		}

		if (para.abs_diff)
		{
			lcg_dot(residual, sk, sk);
			residual = sqrt(residual)/n_size;
			if (Pfp != nullptr)
			{
				if (Pfp(instance, m, residual, &para, t))
				{
					ret = LCG_STOP; return ret;
				}
			}

			if (residual <= para.epsilon)
			{
				for (i = 0; i < n_size; i++)
				{
					m[i] += ak*pk[i];
					if (m[i] != m[i])
					{
						ret = LCG_NAN_VALUE; return ret;
					}
				}
				ret = LCG_CONVERGENCE; return ret;
			}

			if (para.max_iterations > 0 && t+1 > para.max_iterations)
			{
				ret = LCG_REACHED_MAX_ITERATIONS;
				break;
			}
			
			t++;
		}

		Afp(instance, sk, Ax);
		Ass = AsAs = 0.0;
		for (i = 0; i < n_size; i++)
		{
			Ass  += Ax[i]*sk[i];
			AsAs += Ax[i]*Ax[i];
		}
		wk = Ass/AsAs;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] += ak*pk[i] + wk*sk[i];
		}

		for (i = 0; i < n_size; i++)
		{
			if (m[i] != m[i])
			{
				ret = LCG_NAN_VALUE; return ret;
			}
		}

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			rk[i] = sk[i] - wk*Ax[i];
		}

		lcg_dot(rk_mod, rk, rk);

		rkr0_T1 = 0.0;
		for (i = 0; i < n_size; i++)
		{
			rkr0_T1 += rk[i]*r0_T[i];
		}

		rr1_abs = fabs(rkr0_T1);
		if (rr1_abs < para.restart_epsilon)
		{
			for (i = 0; i < n_size; i++)
			{
				r0_T[i] = rk[i];
				pk[i] = rk[i];
			}

			rkr0_T1 = 0.0;
			for (i = 0; i < n_size; i++)
			{
				rkr0_T1 += rk[i]*r0_T[i];
			}
			betak = (ak/wk)*rkr0_T1/rkr0_T;
			rkr0_T = rkr0_T1;
		}
		else
		{
			betak = (ak/wk)*rkr0_T1/rkr0_T;
			rkr0_T = rkr0_T1;

#pragma omp parallel for private (i) schedule(guided)
			for (i = 0; i < n_size; i++)
			{
				pk[i] = rk[i] + betak*(pk[i] - wk*Apk[i]);
			}
		}
	}

	return ret;
}

/**
 * @brief      Conjugate gradient method with projected gradient for inequality constraints.
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param[in]  low         The lower boundary of the acceptable solution.
 * @param[in]  hig         The higher boundary of the acceptable solution.
 * @param[in]  n_size      Size of the solution vector and objective vector.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 * @param      solver_id   Solver type used to solve the linear system. The default value is LCG_CGS.
 * @param      P           Precondition vector (optional expect for the LCG_PCG method). The default value is nullptr.
 *
 * @return     Status of the function.
 */
int lpg(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_array &low, const lcg_array &hig, const lcg_para* param, void* instance)
{
	// set CG parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	// check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0) return LCG_INVILAD_EPSILON;
	if (para.step <= 0.0 || para.epsilon >= 1.0) return LCG_INVALID_LAMBDA;

	// locate memory
	lcg_array gk, Adk;
	lcg_array m_new, gk_new;
	lcg_array sk, yk;
	gk.resize(n_size);
	Adk.resize(n_size);
	m_new.resize(n_size);
	gk_new.resize(n_size);
	sk.resize(n_size);
	yk.resize(n_size);
	lcg_float alpha_k = para.step;

	int i;
	// project the initial model
#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		m[i] = lcg_set2box(low[i], hig[i], m[i]);
	}

	Afp(instance, m, Adk);

#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		gk[i] = Adk[i] - B[i];
	}

	lcg_float gk_mod;
	lcg_dot(gk_mod, gk, gk);

	lcg_float g0_mod = gk_mod;
	if (g0_mod < 1.0) g0_mod = 1.0;

	int ret, t = 0;
	if (para.abs_diff && sqrt(gk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(gk_mod)/n_size, &para, 0);
		}
		return ret;
	}	
	else if (gk_mod/g0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, gk_mod/g0_mod, &para, 0);
		}
		return ret;
	}

	lcg_float sk_mod, syk_mod, residual;
	while(1)
	{
		if (para.abs_diff) residual = sqrt(gk_mod)/n_size;
		else residual = gk_mod/g0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}
		
		t++;

		// project the model
#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m_new[i] = lcg_set2box(low[i], hig[i], m[i] - alpha_k*gk[i]);
		}

		Afp(instance, m_new, Adk);

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			gk_new[i] = Adk[i] - B[i];
			sk[i] = m_new[i] - m[i];
			yk[i] = gk_new[i] - gk[i];
		}

		sk_mod = 0.0;
		syk_mod = 0.0;
		for (i = 0; i < n_size; i++)
		{
			sk_mod += sk[i]*sk[i];
			syk_mod += sk[i]*yk[i];
		}
		alpha_k = sk_mod/syk_mod;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] = m_new[i];
			gk[i] = gk_new[i];
		}

		lcg_dot(gk_mod, gk, gk);
	}

	return ret;
}

/**
 * @brief      Conjugate gradient method with projected gradient for inequality constraints.
 *
 * @param[in]  Afp         Callback function for calculating the product of 'Ax'.
 * @param[in]  Pfp         Callback function for monitoring the iteration progress.
 * @param      m           Initial solution vector.
 * @param      B           Objective vector of the linear system.
 * @param[in]  low         The lower boundary of the acceptable solution.
 * @param[in]  hig         The higher boundary of the acceptable solution.
 * @param[in]  n_size      Size of the solution vector and objective vector.
 * @param      param       Parameter setup for the conjugate gradient methods.
 * @param      instance    The user data sent for the lcg_solver() function by the client. 
 * This variable is either 'this' for class member functions or 'nullptr' for global functions.
 * @param      solver_id   Solver type used to solve the linear system. The default value is LCG_CGS.
 * @param      P           Precondition vector (optional expect for the LCG_PCG method). The default value is nullptr.
 *
 * @return     Status of the function.
 */
int lspg(lcg_axfunc_ptr Afp, lcg_progress_ptr Pfp, lcg_array &m, const lcg_array &B, 
	const lcg_array &low, const lcg_array &hig, const lcg_para* param, void* instance)
{
	// set CG parameters
	lcg_para para = (param != nullptr) ? (*param) : defparam;

	size_t n_size = B.size();
	// check parameters
	if (n_size <= 0) return LCG_INVILAD_VARIABLE_SIZE;
	if (n_size != m.size()) return LCG_SIZE_NOT_MATCH;
	if (para.max_iterations < 0) return LCG_INVILAD_MAX_ITERATIONS;
	if (para.epsilon <= 0.0 || para.epsilon >= 1.0) return LCG_INVILAD_EPSILON;
	if (para.step <= 0.0) return LCG_INVALID_LAMBDA;
	if (para.sigma <= 0.0 || para.sigma >= 1.0) return LCG_INVALID_SIGMA;
	if (para.beta <= 0.0 || para.beta >= 1.0) return LCG_INVALID_BETA;
	if (para.maxi_m <= 0) return LCG_INVALID_MAXIM;

	// locate memory
	lcg_array gk, Adk;
	lcg_array m_new, gk_new;
	lcg_array sk, yk;
	lcg_array dk, qk_m;
	gk.resize(n_size);
	Adk.resize(n_size);
	m_new.resize(n_size);
	gk_new.resize(n_size);
	sk.resize(n_size);
	yk.resize(n_size);
	dk.resize(n_size);
	qk_m.resize(para.maxi_m);
	lcg_float lambda_k = para.step;
	lcg_float qk = 0;

	int i;
	// project the initial model
#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		m[i] = lcg_set2box(low[i], hig[i], m[i]);
	}

	Afp(instance, m, Adk);

#pragma omp parallel for private (i) schedule(guided)
	for (i = 0; i < n_size; i++)
	{
		gk[i] = Adk[i] - B[i];
	}

	lcg_float gk_mod;
	lcg_dot(gk_mod, gk, gk);

	lcg_float g0_mod = gk_mod;
	if (g0_mod < 1.0) g0_mod = 1.0;

	int ret, t = 0;
	if (para.abs_diff && sqrt(gk_mod)/n_size <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, sqrt(gk_mod)/n_size, &para, 0);
		}
		return ret;
	}	
	else if (gk_mod/g0_mod <= para.epsilon)
	{
		ret = LCG_ALREADY_OPTIMIZIED;
		if (Pfp != nullptr)
		{
			Pfp(instance, m, gk_mod/g0_mod, &para, 0);
		}
		return ret;
	}

	// calculate qk
	for (i = 0; i < n_size; i++)
	{
		qk += (0.5*m[i]*Adk[i] - B[i]*m[i]);
	}
	qk_m[0] = qk;

	for (i = 1; i < para.maxi_m; i++)
	{
		qk_m[i] = -1e+30;
	}

	lcg_float alpha_k, maxi_qk, residual;
	lcg_float alpha_mod, sk_mod, syk_mod;
	while(1)
	{
		if (para.abs_diff) residual = sqrt(gk_mod)/n_size;
		else residual = gk_mod/g0_mod;

		if (Pfp != nullptr)
		{
			if (Pfp(instance, m, residual, &para, t))
			{
				ret = LCG_STOP; return ret;
			}
		}

		if (residual <= para.epsilon)
		{
			ret = LCG_CONVERGENCE; return ret;
		}

		if (para.max_iterations > 0 && t+1 > para.max_iterations)
		{
			ret = LCG_REACHED_MAX_ITERATIONS;
			break;
		}

		t++;

		// project the model
#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			dk[i] = lcg_set2box(low[i], hig[i], m[i] - lambda_k*gk[i]) - m[i];
		}

		alpha_k = 1.0;
		for (i = 0; i < n_size; i++)
		{
			m_new[i] = m[i] + alpha_k*dk[i];
		}

		Afp(instance, m_new, Adk);

		qk = 0.0;
		for (i = 0; i < n_size; i++)
		{
			qk += (0.5*m_new[i]*Adk[i] - B[i]*m_new[i]);
		}

		alpha_mod = 0.0;
		for (i = 0; i < n_size; i++)
		{
			alpha_mod += para.sigma*alpha_k*gk[i]*dk[i];
		}

		maxi_qk = qk_m[0];
		for (i = 1; i < para.maxi_m; i++)
		{
			maxi_qk = lcg_max(maxi_qk, qk_m[i]);
		}

		while(qk > maxi_qk + alpha_mod)
		{
			alpha_k = alpha_k*para.beta;

			for (i = 0; i < n_size; i++)
			{
				m_new[i] = m[i] + alpha_k*dk[i];
			}

			Afp(instance, m_new, Adk);

			qk = 0.0;
			for (i = 0; i < n_size; i++)
			{
				qk += (0.5*m_new[i]*Adk[i] - B[i]*m_new[i]);
			}

			alpha_mod = 0.0;
			for (i = 0; i < n_size; i++)
			{
				alpha_mod += para.sigma*alpha_k*gk[i]*dk[i];
			}
		}

		// put new values in qk_m
		qk_m[(t+1)%para.maxi_m] = qk;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			gk_new[i] = Adk[i] - B[i];
			sk[i] = m_new[i] - m[i];
			yk[i] = gk_new[i] - gk[i];
		}

		sk_mod = 0.0;
		syk_mod = 0.0;
		for (i = 0; i < n_size; i++)
		{
			sk_mod += sk[i]*sk[i];
			syk_mod += sk[i]*yk[i];
		}
		lambda_k = sk_mod/syk_mod;

#pragma omp parallel for private (i) schedule(guided)
		for (i = 0; i < n_size; i++)
		{
			m[i] = m_new[i];
			gk[i] = gk_new[i];
		}

		lcg_dot(gk_mod, gk, gk);
	}

	return ret;
}